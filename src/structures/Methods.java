package structures;
        
public class Methods {
    
    //This is methods
    /**********
    Funciones:
    ordenarLista(SLList, SSList) ordena una lista de mayor a menor, se explica más adelante
    toInt(Object) devuelve el valor del objeto en un int.Si el objeto no es de clase Int, devuelve 0
    toString(Object) devuelve el valor del objeto en un String. Si el objeto no es de clase String, devuelve "" (string vacía)
    invertirLista(SLList) devuelve la lista invertida
    invertirStack(Stack) devuelve el stack invertido
    invertirQueue(Queue) devuelve el queue invertido
    **********/
    
    /*********
    Función findData(SLList list, Object data)
    Devuelve un arreglo de enteros, cada entero del arreglo es una posición de la lista "list" en la cual se encontró el dato.
    Para usarla, usar el siguiente código:
            
            SLList listIntegers = new SLList();
            SLListNode node = list.getHead();
            while (node != null){
                Class obj = (Class)node.data;
                listIntegers.insertTail(0, (int)obj.(°°°));
                node = node.next;
            }
        
        reemplazar Class por la clase de los objetos de la lista ( ejemplo: lista de clientes  ->  Client obj = (Client)node.data; )
        reemplazar (°°°) por el atributo entero que se desea comparar ( ejemplo: edad de un cliente  ->  (int)obj.age )
        luego se llama a la función así: methods.findData(listData, data); donde "data" es el dato Object que se desea buscar
            
    **********/
    
    public int[] findData(SLList list, Object data) {
        
        if (data == null || list.head == null) {
            return null;
        }
        
        Queue pos = new Queue();
        
        for (int i = 0; i < list.getSize(); i++) {
            if (list.readNode(i) == data) {
                pos.push(0, i);
            }
        }
        
        int[] positions = new int[pos.getSize()];
        
        for (int i = 0; pos.getHead() != null; i++) {
            positions[i] = toInt(pos.read());
            pos.pop();
        }
        
        return positions;
        
    }
    
    /**********
    Función ordenarLista(SLList listOriginal, SLList listKeys)
    Devuelde una lista organizada. Funciona como se explica a continuación (methods es un objeto de clase Methods):
    
    * Ordenar lista "list" por keys: los parámetros que se pasan a la función son la lista "list" dos veces: methods.ordenarLista(list, list)
    
    * ordenar lista "list" por atributo diferente a keys: usar el siguiente código:
    
            SLList listIntegers = new SLList();
            SLListNode node = list.getHead();
            while (node != null){
                Class obj = (Class)node.data;
                listIntegers.insertTail((int)obj.(°°°), null);
                node = node.next;
            }
    
        reemplazar Class por la clase de los objetos de la lista ( ejemplo: lista de clientes  ->  Client obj = (Client)node.data; )
        reemplazar (°°°) por el atributo entero que se desea comparar ( ejemplo: edad de un cliente  ->  (int)obj.age )
        luego se llama a la función así: methods.ordenarLista(list, listIntegers);
    
    **********/
    
    public SLList ordenarLista(SLList listOriginal, SLList listKeys){
        
        if (listOriginal.getHead() == null || listKeys.getHead() == null) {
            return null;
        }
        
        if (listOriginal.equals(listKeys)) {
            SLList list = listKeys;
            list.head = ordenamiento(list.head);
            list.restoreTail();
            SLList newList = new SLList();
            for (int i = 0; i < listOriginal.getSize(); i++) {
                int pos = listKeys.findUsingKey(list.readNodeKey(i));
                newList.insertTail(listOriginal.readNodeKey(pos), listOriginal.readNode(pos));
                listKeys.removeAt(pos);
                listKeys.insertAt(-1, null, pos);
            }
            return newList;
        } else {
            SLList listaIndices = new SLList();
            for (int i = 0; i < listKeys.getSize(); i++) {
            listaIndices.insertTail(listKeys.readNodeKey(i), listKeys.readNode(i));
            }
            SLList list = listKeys;
            list.head = ordenamiento(list.head);
            list.restoreTail();
            SLList newList = new SLList();
            for (int i = 0; i < listOriginal.getSize(); i++) {
                int pos = listaIndices.findUsingKey(list.readNodeKey(i));
                newList.insertTail(listOriginal.readNodeKey(pos), listOriginal.readNode(pos));
                listaIndices.removeAt(pos);
                listaIndices.insertAt(-1, null, pos);
            }
            return newList;
        }
    }
    
    private SLListNode ordenamiento(SLListNode head) {
        
        if(head == null || head.next == null) {
            return head;
        }
        
        SLListNode medio;
        
        SLListNode atraso = head; 
        SLListNode adelanto = head; 
        while(adelanto.next != null && adelanto.next.next != null) {
            atraso = atraso.next;
            adelanto = adelanto.next.next;
        }
        medio = atraso;
        
        SLListNode mitad = medio.next;
        medio.next = null;

        return unir(ordenamiento(head), ordenamiento(mitad));
    }

    private SLListNode unir(SLListNode izq, SLListNode der) {
        SLListNode node = new SLListNode(0, null); 
        SLListNode temp = node;
        while(izq !=null && der!= null) {
            if(izq.key <= der.key) {
                temp.next = izq; izq = izq.next;
            }
            else {
                temp.next = der; der = der.next;
            }
            temp = temp.next;
        }
        temp.next = (izq == null) ? der : izq;
        return node.next;
    }
    
    public int toInt(Object obj) {
        if (obj != null) {
            if (obj instanceof Integer) {
                return (int)obj;
            }
        }
        return 0;
    }
    
    public String toString(Object obj) {
        if (obj != null) {
            if (obj instanceof String) {
                return (String)obj;
            }
        }
        return null;
    }
    
    public SLList invertirLista (SLList list){
        if (list == null || list.getHead() == null) {
            return list;
        }
        SLList newList = new SLList();
        while (list.getHead() != null || list.getHead() == null) {
            newList.insertHead(list.readHeadKey(), list.readHead());
            list.removeHead();
        }
        return newList;
    }
    
    public Stack invertirStack (Stack stack) {
        if (stack == null || stack.getHead() == null) {
            return stack;
        }
        Stack newStack = new Stack();
        while (stack.getHead() != null) {
            newStack.push(stack.readKey(), stack.read());
            stack.pop();
        }
        return newStack;
    }
    
    public Queue invertirQueue (Queue queue){
        if (queue == null || queue.getHead() == null) {
            return queue;
        }
        SLList list = new SLList(queue);
        queue.makeEmpty();
        while (list.getTail() != null) {
            queue.push(queue.readKey(), list.readTail());
            list.removeTail();
        }
        return queue;
    }
    
}
