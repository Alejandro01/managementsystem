package structures;

import java.io.Serializable;


public class SLListNode implements Serializable{

    public Object data;
    public SLListNode next;
    public int key;

    public SLListNode(int key, Object nodeData) {
        this.data = nodeData;
        this.next = null;
        this.key = key;
    }

}
