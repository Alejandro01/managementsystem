package structures;

import Data.Client;
import java.io.Serializable;

public class SLList implements Serializable {

    public SLListNode head;
    public SLListNode tail;
    public int size;

    /**
     * ********
     * Constructores: SLList() constructor convencional SLList(Stack) lista
     * nueva copia elementos de stack SLList(Queue) lista nueva copia elementos
     * de queue ********
     */
    public SLList() {
        this.head = null;
        this.tail = null;
        this.size = 0;
    }

    public SLList(Stack stack) {
        this.head = stack.head;
        if (this.head != null) {
            SLListNode node = this.head;
            while (node.next != null) {
                node = node.next;
            }
            this.tail = node;
        } else if (this.head == null) {
            this.tail = null;
        }
        this.size = stack.size;
    }

    public SLList(Queue queue) {
        this.head = queue.head;
        this.tail = queue.tail;
        this.size = queue.size;
    }

    /**
     * ********
     * Datos: getters para size, head y tail setters para head y tail setAt(key,
     * data) busca el objeto con la key dada y sobreescribe su data con la data
     * dada restoreSize() restaura el tamaño de la lista al valor real
     * restoreTail() restaura la cola de la lista al valor real find(data)
     * retorna la posición del elemento buscado, si no lo encuentra retorna -1
     * findUsingKey(key) retorna la posición de la key buscada, si no la
     * encuentra retorna -1 readNode(position) retorna el dato del nodo en la
     * posición dada readHead() retorna el dato de la cabeza readTail() retorna
     * el dato de la cola readNodeKey(position), readHeadKey() y readTailKey()
     * devuelven key printList() imprime la lista (data) en la consola
     * printListKeys() imprime la lista (keys) en la consola ********
     */
    public int getSize() {
        return this.size;
    }

    public SLListNode getHead() {
        return this.head;
    }

    public SLListNode getTail() {
        return this.tail;
    }

    public void setHead(SLListNode head) {
        this.head = head;
        restoreSize();
        restoreTail();
    }

    public void setTail(SLListNode tail) {
        this.tail = tail;
        restoreSize();
        restoreTail();
    }

    public void setAt(int key, Object data) {
        SLListNode current = this.head;
        for (int i = 0; i < size; i++) {
            if (current.key == key) {
                current.data = data;
                return;
            }
            current = current.next;
        }
    }

    public void restoreSize() {
        this.size = 0;
        SLListNode node = this.head;
        while (node != null) {
            size++;
            node = node.next;
        }
    }

    public void restoreTail() {
        SLListNode node = this.head;
        while (node.next != null) {
            node = node.next;
        }
        this.tail = node;
    }

    public int find(Object data) {
        SLListNode node = this.head;
        for (int i = 0; i < size; i++) {
            if (data.equals(node.data)) {
                return i;
            }
            node = node.next;
        }
        return -1;
    }

    public int findUsingKey(int key) {
        SLListNode node = this.head;
        for (int i = 0; i < size; i++) {
            if (node.key == key) {
                return i;
            }
            node = node.next;
        }
        return -1;
    }

    public Object readNode(int position) {
        if (position < this.size && position >= 0) {
            SLListNode node = this.head;
            for (int i = 0; i < position; i++) {
                node = node.next;
            }
            return node.data;
        }
        return null;
    }

    public Object readHead() {
        if (this.head != null) {
            return this.head.data;
        }
        return null;
    }

    public Object readTail() {
        if (this.tail != null) {
            return this.tail.data;
        }
        return null;
    }

    public int readNodeKey(int position) {
        if (position < this.size && position >= 0) {
            SLListNode node = this.head;
            for (int i = 0; i < position; i++) {
                node = node.next;
            }
            return node.key;
        }
        return 0;
    }

    public int readHeadKey() {
        if (this.head != null) {
            return this.head.key;
        }
        return 0;
    }

    public int readTailKey() {
        if (this.tail != null) {
            return this.tail.key;
        }
        return 0;
    }

    public void printList() {
        if (this.head != null) {
            SLListNode node = this.head;
            while (node != null) {
                System.out.print(node.data + " ");
                node = node.next;
            }
            System.out.print("\n");
        }
        return;
    }

    public void printListKeys() {
        if (this.head != null) {
            SLListNode node = this.head;
            while (node != null) {
                System.out.print(node.key + " ");
                node = node.next;
            }
            System.out.print("\n");
        }
        return;
    }

    /**
     * ********
     * Insertar nodos insertHead(key, data) inserta nodo en la cabeza con el
     * dato dado insertTail(key, data) inserta nodo en la cola con el dato dado
     * insertAt(key, data, position) inserta nodo en la posicion dada con el
     * dato dado ********
     */
    public void insertHead(int key, Object data) {
        SLListNode node = new SLListNode(key, data);
        if (this.head == null) {
            this.tail = node;
        }
        node.next = this.head;
        this.head = node;
        this.size++;
        return;
    }

    public void insertTail(int key, Object data) {
        SLListNode node = new SLListNode(key, data);
        if (this.head == null) {
            this.head = node;
        } else {
            this.tail.next = node;
        }
        this.tail = node;
        this.size++;
        return;
    }

    public void insertAt(int key, Object data, int position) {
        if (position == 0) {
            insertHead(key, data);
        } else if (position == this.size) {
            insertTail(key, data);
        } else if (position < this.size && position > 0) {
            SLListNode node = new SLListNode(key, data);
            SLListNode nodePrev = this.head;
            for (int i = 0; i < position; i++) {
                if (position - 1 == i) {
                    node.next = nodePrev.next;
                    nodePrev.next = node;
                    this.size++;
                    break;
                }
                nodePrev = nodePrev.next;
            }
        }
        return;
    }

    /**
     * ********
     * Remover nodos makeEmpty() vacía la lista removeHead() remueve el primer
     * nodo de la lista removeTail() remueve el último nodo de la lista
     * removeAt(posicion) remueve el nodo de la posición dada ********
     */
    public void makeEmpty() {
        this.head = null;
        this.tail = null;
        this.size = 0;
    }

    public void removeHead() {
        if (this.head != null) {
            this.head = this.head.next;
            this.size--;
        }
        if (this.head == null) {
            this.tail = null;
            this.size = 0;
        }
    }

    public void removeTail() {
        if (this.tail == this.head) {
            removeHead();
            return;
        }
        if (this.tail != null) {
            SLListNode nodePrev = this.head;
            while (nodePrev.next != this.tail) {
                nodePrev = nodePrev.next;
            }
            nodePrev.next = null;
            this.tail = nodePrev;
            this.size--;
        }
    }

    public void removeAt(int position) {
        if (position > 0 && position < size - 1) {
            SLListNode nodePrev = this.head;
            for (int i = 0; i < position - 1; i++) {
                nodePrev = nodePrev.next;
            }
            nodePrev.next = nodePrev.next.next;
            this.size--;
        } else {
            if (position == 0) {
                removeHead();
            } else {
                if (position == size - 1) {
                    removeTail();
                }
            }
        }
    }

}
