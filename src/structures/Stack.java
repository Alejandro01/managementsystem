package structures;

import java.io.Serializable;

public class Stack implements Serializable{
    
    /**********
    Nota: En este stack los elementos ingresan por head y salen por head
    **********/
      
    public SLListNode head;
    public int size;
    
    /**********
    Constructores:
    Stack() constructor convencional
    Stack(SLList) stack nuevo copia elementos de lista
    Stack(Queue) stack nuevo copia elementos de queue
    **********/
    
    public Stack() {
        this.head = null;
        this.size = 0;
    }
    
    public Stack (SLList sllist) {
        this.head = sllist.head;
        this.size = sllist.size;
    }
    
    public Stack (Queue queue) {
        this.head = queue.head;
        this.size = queue.size;
    }
    
    /**********
    Datos:
    getters para size y head
    setter para head
    find(data) retorna verdadero si encuentra el elemento, retorna falso de lo contrario
    findUsingKey(key) retorna verdadero si encuentra la key, retorna falso de lo contrario
    read() retorna el dato de la cabeza
    readKey() devuelve key de la cabeza
    printStack() imprime el stack en la consola
    **********/
    
    public int getSize() {
        return this.size;
    }
    
    public SLListNode getHead() {
        return this.head;
    }

    public void setHead(SLListNode head) {
        this.head = head;
        restoreSize();
    }
    
    public void restoreSize(){
        this.size = 0;
        SLListNode node = this.head;
        while (node != null) {
            size++;
            node = node.next;
        }
    }

    public boolean find(Object data) {
        if (this.head != null) {
            SLListNode node = this.head;
            while (node != null) {
                if (data == node.data) {
                    return true;
                }
                node = node.next;
            }
        }
        
       return false;
    }
    
    public boolean findUsingKey(int key) {
        SLListNode node = this.head;
        for (int i = 0; i < size; i++) {
            if (node.key == key) {
                return true;
            }
            node = node.next;
         }
        return false;
    }
    
    public Object read() {
        if (this.head != null) {
            return this.head.data;
        }
        return null;
    }
    
    public int readKey() {
        if (this.head != null) {
            return this.head.key;
        }
        return 0;
    }
    
    public void printStack() {
        if (this.head != null) {
            SLListNode node = this.head;
            while (node != null) {
                System.out.print(node.data + " ");
                node = node.next;
            }
            System.out.print("\n");
        }
        return;
    }
    
    /**********
    Insertar nodos
    push(key, data) inserta nodo en la cabeza
    **********/
    
    public void push(int key, Object data) {
        SLListNode node = new SLListNode(key, data);
        node.next = this.head;
        this.head = node;
        this.size++;
        return;
    }
    
    /**********
    Remover nodos
    makeEmpty() vacía el stack
    pop() remueve la cabeza del stack
    **********/
    
    public void makeEmpty() {
        this.head = null;
        this.size = 0;
        return;
    }

    public void pop() {
        if (this.head != null) {
            this.head = this.head.next;
            this.size--;
        }
        return;
    }
    
}
