package structures;

import java.io.Serializable;

public class Queue implements Serializable{
    
    /**********
    Nota: En este queue los elementos ingresan por tail y salen por head
    **********/
    
    public SLListNode head;
    public SLListNode tail;
    public int size;
    private boolean bool;
    
    /**********
    Constructores:
    Queue() constructor convencional
    Queue(Stack) queue nuevo copia elementos de stack
    Queue(SLList) queue nuevo copia elementos de lista
    **********/
    
    public Queue() {
        this.head = null;
        this.tail = null;
        this.size = 0;
    }
    
    public Queue (Stack stack) {
        this.head = stack.head;
        if (this.head != null) {
            SLListNode node = this.head;
            while (node.next != null) {
                node = node.next;
            }
            this.tail = node;
        } else if (this.head == null) {
            this.tail = null;
        }
        this.size = stack.size;
    }
    
    public Queue (SLList sllist) {
        this.head = sllist.head;
        this.tail = sllist.tail;
        this.size = sllist.size;
    }
    
    /**********
    Datos:
    getters para size, head y tail
    setters para head y tail
    getTail() retorna la cola del stack
    find(data) retorna verdadero si encuentra el elemento, retorna falso de lo contrario
    findUsingKey(key) retorna verdadero si encuentra la key, retorna falso de lo contrario
    read() retorna el dato de la cabeza
    readKey() devuelve key de la cabeza
    printQueue() imprime el queue en la consola
    **********/
    
    public int getSize() {
        return this.size;
    }
    
    public SLListNode getHead() {
        return this.head;
    }

    public SLListNode getTail() {
        return this.tail;
    }

    public void setHead(SLListNode head) {
        this.head = head;
        restoreSize();
        restoreTail();
    }
    
    public void setTail(SLListNode tail) {
        this.tail = tail;
        restoreSize();
        restoreTail();
    }
    
    public void restoreSize(){
        this.size = 0;
        SLListNode node = this.head;
        while (node != null) {
            size++;
            node = node.next;
        }
    }
    
    public void restoreTail(){
        SLListNode node = this.head;
        while (node.next != null) {
            node = node.next;
        }
        this.tail = node;
    }

    public boolean find(Object data) {
        if (this.head != null) {
            SLListNode node = this.head;
            while (node != null) {
                if (data == node.data) {
                    return true;
                }
                node = node.next;
            }
        }
        
       return false;
    }
    
    public boolean findUsingKey(int key) {
        SLListNode node = this.head;
        for (int i = 0; i < size; i++) {
            if (node.key == key) {
                return true;
            }
            node = node.next;
         }
        return false;
    }
    
    public Object read() {
        if (this.head != null) {
            return this.head.data;
        }
        return null;
    }
    
    public int readKey() {
        if (this.head != null) {
            return this.head.key;
        }
        return 0;
    }
    
    public void printQueue() {
        if (this.head != null) {
            SLListNode node = this.head;
            while (node != null) {
                System.out.print(node.data + " ");
                node = node.next;
            }
            System.out.print("\n");
        }
        return;
    }
    
    /**********
    Insertar nodos
    push(key, data) inserta nodo en la cola
    **********/
    
    public void push(int key, Object data) {
        SLListNode node = new SLListNode(key, data);
        if (this.head == null) {
            this.head = node;
        } else {
            this.tail.next = node;
        }
        this.tail = node;
        this.size++;
        return;
    }
    
    /**********
    Remover nodos
    makeEmpty() vacía el queue
    pop() remueve la cabeza del queue
    **********/
    
    public void makeEmpty() {
        this.head = null;
        this.tail = null;
        this.size = 0;
        return;
    }

    public void pop() {
        if (this.head != null) {
            this.head = this.head.next;
            this.size--;
        }
        if (this.head == null) {
            this.tail = null;
        }
        return;
    }
    
}
