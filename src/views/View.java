package views;

import Data.Cart;
import Data.Client;
import Data.Direction;
import Data.Employee;
import java.util.Scanner;
import javax.sound.midi.SysexMessage;
import structures.SLList;

public class View {

    Scanner reader = new Scanner(System.in);

    public void welcome() {
        System.out.println("                                    ¡¡Bienvenido a tiendas online!!  ");
    }

    public int getRolOption() {
        System.out.println("Introduzca alguna de las opciones: ");
        System.out.println("1) Rol Empleado");
        System.out.println("2) Rol cliente");
        return Integer.parseInt(reader.nextLine());
    }

    public int getMenuOptionEmployee() {
        System.out.println("-------------------------------------- Pagina del Empleado --------------------------------------");
        System.out.println("Introduzca alguna de las opciones: ");
        System.out.println("1) Para ingresar ");
        System.out.println("2) Para regresar ");
        return Integer.parseInt(reader.nextLine());
    }
    
    public int getMenuOptionClient() {
        System.out.println("-------------------------------------- Pagina del Cliente --------------------------------------");
        System.out.println("Introduzca alguna de las opciones: ");
        System.out.println("1) Para ingresar ");
        System.out.println("2) Para crear un usuario ");
        System.out.println("3) Para regresar ");
        return Integer.parseInt(reader.nextLine());
    }

    public int getControllerEmployeeMenuOption(){
        System.out.println("-------------------------------------- Controlador de Empleados --------------------------------------");
        System.out.println("Introduzca alguna de las opciones: ");
        System.out.println("1) Para crear un empleado ");
        System.out.println("2) Para ver la información de un empleado ");
        System.out.println("3) Para actualizar la información de un empleado ");
        System.out.println("4) Para eliminar un empleado ");
        System.out.println("5) Para salir ");
        return Integer.parseInt(reader.nextLine());
    }
    
    public int getControllerClientMenuOption() {
        System.out.println("-------------------------------------- Controlador de Clientes --------------------------------------");
        System.out.println("Introduzca alguna de las opciones: ");
        System.out.println("1) Para crear un cliente ");
        System.out.println("2) Para ver la información de un cliente ");
        System.out.println("3) Para actualizar la información de un cliente ");
        System.out.println("4) Para eliminar un cliente ");
        System.out.println("5) Para salir ");
        return Integer.parseInt(reader.nextLine());
    }

    public int getSortClientMenuOption(){
        System.out.println("-------------------------------------- Ordenador de Clientes --------------------------------------");
        System.out.println("Introduzca alguna de las opciones: ");
        System.out.println("1) Para ordenar clientes por edad ");
        System.out.println("2) Para ordenar clientes por distancia a la dirección de la empresa");
        System.out.println("3) Para ordenar clientes por orden alfabetico del nombre ");
        System.out.println("4) Para salir ");
        return Integer.parseInt(reader.nextLine());
    }
    
    public void printErrorUser() {
        System.out.println("Usuario incorrecto");
    }

    public void printErrorMessage() {
        System.out.println("No seleccionó ninguna de las opciones");
    }

    public String getCompanyName() {
        System.out.println("Introduzca nombre de la compañia");
        return reader.nextLine();
    }

    public String getCompanyMail() {
        System.out.println("Introduzca correo de la compañia");
        return reader.nextLine();
    }
    // Ingreso de datos Client
    public String getName() {
        System.out.println("Introduzca nombre: ");
        return reader.nextLine();
    }

    public String getLastName() {
        System.out.println("Introduzca apellido: ");
        return reader.nextLine();
    }

    public String getUsername() {
        System.out.println("Introduzca su nombre de usuario: ");
        return reader.nextLine();
    }

    public String getPassword() {
        System.out.println("Introduzca su contraseña: ");
        return reader.nextLine();
    }

    public int getAge() {
        System.out.println("Introduzca su edad: ");
        return Integer.parseInt(reader.nextLine());
    }

    public int getCompanyStreetPerpendicular() {
        System.out.println("Introduzca el número de la carrera de la dirección de la compañia: ");
        return Integer.parseInt(reader.nextLine());
    }

    public int getCompanyStreet() {
        System.out.println("Introduzca el número de la calle de la dirección de la compañia: ");
        return Integer.parseInt(reader.nextLine());
    }

    public int getStreetPerpendicular() {
        System.out.println("Introduzca el número de la carrera de la dirección de la persona: ");
        return Integer.parseInt(reader.nextLine());
    }

    public int getStreet() {
        System.out.println("Introduzca el número de la calle de la dirección de la persona: ");
        return Integer.parseInt(reader.nextLine());
    }

    public int getPhoneNumber() {
        System.out.println("Introduzca el numero de telefono: ");
        return Integer.parseInt(reader.nextLine());
    }

    public String getEmail() {
        System.out.println("Introduzca el correo electronico: ");
        return reader.nextLine();
    }

    public String getSex() {
        System.out.println("Introduzca el sexo: ");
        return reader.nextLine();
    }

    public int getId() {
        System.out.println("Ingrese el Id: ");
        return Integer.parseInt(reader.nextLine());
    }

    public String getHiringDate() {
        System.out.println("Ingrese el día contratación del empleado (si el día es menor a 10 poner por ejemplo 07): ");
        String date = reader.nextLine() + "/";
        System.out.println("Ingrese el mes contratación del empleado (si el mes es menor a 10 poner por ejemplo 07): ");
        date += reader.nextLine() + "/";
        System.out.println("Ingrese el año contratación del empleado: ");
        date += reader.nextLine();
        return date;
    }

    public String getEmployeePosition() {
        System.out.println("Ingrese el cargo del empleado: ");
        return reader.nextLine();
    }

    public String getDriverState() {
        System.out.println("Introduzca el estado de contratación del conductor: ");
        return reader.nextLine();
    }

    public int getTruckPlateNumber() {
        System.out.println("Ingrese la parte numerica de la placa del tractocamion mnejado por el conductor: ");
        return Integer.parseInt(reader.nextLine());
    }

    public int getTruckPlateLetter() {
        System.out.println("Ingrese la parte alfabetica de la placa del tractocamion mnejado por el conductor: ");
        return Integer.parseInt(reader.nextLine());
    }

    public String getTruckBrand() {
        System.out.println("Ingrese la parte alfabetica de la placa del tractocamion mnejado por el conductor: ");
        return reader.nextLine();
    }

    public int getSalary() {
        System.out.println("Ingrese el salario del empleado: ");
        return Integer.parseInt(reader.nextLine());
    }

    public String getEngagementType() {
        System.out.println("Ingrese el tipo de contrato: ");
        return reader.nextLine();
    }

    public String getProductCategory() {
        System.out.println("Ingrese la categoría del producto: ");
        return reader.nextLine();
    }

    public String getProductDescription() {
        System.out.println("Ingrese una descripción del producto: ");
        return reader.nextLine();
    }

    public int getProductAssurance() {
        System.out.println("Ingrese el tiempo de garantía del producto en meses: ");
        return Integer.parseInt(reader.nextLine());
    }

    public int getPrice() {
        System.out.println("Ingrese el precio en pesos Colombianos: ");
        return Integer.parseInt(reader.nextLine());
    }

    public String getCartState() {
        System.out.println("Ingrese el estado del carrito: ");
        return reader.nextLine();
    }

    public int getCartStreetPerpendicular() {
        System.out.println("Introduzca el número de la carrera de la direccion de envio del carrito: ");
        return Integer.parseInt(reader.nextLine());
    }

    public int getCartStreet() {
        System.out.println("Introduzca el número de la calle de la dirección de envio del carrito: ");
        return Integer.parseInt(reader.nextLine());
    }

    public static void requestData() {
        Scanner reader = new Scanner(System.in);
        System.out.println("Introduzca usuario:");
        String username = reader.nextLine();
        System.out.println("Introduzca contraseña:");
        String password = reader.nextLine();
//        Client client = new Client(null, 0, null, null, username, password, 0, null, 0, null, null);
        //      mangementsystem.MangementSystem.check(client);
    }
}
