package mangementsystem;

import Data.Client;
import Data.Company;
import Data.Direction;
import Data.Employee;
import Data.Person;
import Data.Product;
import com.sun.org.apache.bcel.internal.generic.AALOAD;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.AbstractQueue;
import java.util.ArrayList;
import java.util.Date;
import structures.Methods;
import structures.Queue;
import structures.SLList;
import structures.SLListNode;
import structures.Stack;
import views.View;
import mangementsystem.MathematicalFunctions;
/**
 *
 * @author usuario
 */
public class MangementSystem {

    public static void guardarInfo(Company companyp) throws IOException {
        FileOutputStream fileStream = null;

        try {
            fileStream = new FileOutputStream("datos.txt");
            ObjectOutputStream os = new ObjectOutputStream(fileStream);

            os.writeObject(companyp);

        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } finally {
            try {
                fileStream.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }

    }

    public static Company cargarInfo() throws IOException, ClassNotFoundException {
        FileInputStream inputStream = null;
        Company p = null;
        try {
            inputStream = new FileInputStream("datos.txt");
            ObjectInputStream os = new ObjectInputStream(inputStream);

            Object listaObjetos = os.readObject();
            Company usuarios = (Company) listaObjetos;
            p = usuarios;
            return p;
        } catch (Exception e) {

        }
        return p;
    }

    public static int checkClient(String username, String password, Company company) {
        Client client = new Client(null, 0, null, null, username, password, 0, null, 0, null, null);
        SLListNode head = company.getClients().head;
        boolean exist = false;
        if (head != null) {
            SLListNode node = head;
            while (node != null) {
                if (client.equals((Client) node.data)) {
                    return node.key;
                }
                node = node.next;
            }
        }
        return -1;
    }

    public static int checkEmployee(String username, String password, Company company) {
        Employee employee = new Employee(null, 0, null, null, null, null, 0, null, null, username, password, 0, null, 0, null, null);
        SLListNode head = company.getEmployees().head;
        boolean exist = false;
        if (head != null) {
            SLListNode node = head;
            while (node != null) {
                if (employee.equals((Employee) node.data)) {
                    return node.key;
                }
                node = node.next;
            }
        }
        return -1;
    }

    public static void orderClient(Company company, Methods methods, String atributo) {

        SLList listIntegers = new SLList();
        SLListNode node = company.getClients().getHead();
        while (node != null) {
            Client obj = (Client) node.data;
            if (atributo == "AGE") {
                listIntegers.insertTail((int) obj.getAge(), null);
            } else {
                if (atributo == "DISTANCE") {
                    listIntegers.insertTail((int) getDistance(obj.getDirection().getRace(), obj.getDirection().getStreet(), company.getDirection().getRace(), company.getDirection().getStreet()), null);
                } else {
                    listIntegers.insertTail((int) obj.getName().charAt(0), null);
                }
            }
            node = node.next;
        }
        System.out.println("------------------------------- Lista Ordenada -------------------------------");
        methods.ordenarLista(company.getClients(), listIntegers).printList();
    }
    
    public static int getDistance(int raceClient, int streetClient, int x, int y) {
        return MathematicalFunctions.abs(MathematicalFunctions.raiz(MathematicalFunctions.pow((raceClient - x), 2) + MathematicalFunctions.pow((streetClient - y), 2)));
    }
}
