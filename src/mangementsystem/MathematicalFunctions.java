package mangementsystem;

/**
 *
 * @author Usuario
 */
public class MathematicalFunctions {
    
    public static int raiz(int a){
        return (int) Math.sqrt(a);
    }
    
    public static int abs(int n) {
        if (n > 0) {
            return n;
        } else {
            return -n;
        }
    }

    public static int pow(int a, int b) {
        if (b == 0) {
            return 1;
        } else {
            if (b == 1) {
                return 1;
            }else{
                return pow(a, b-1)*a;
            }
        }
    }
}
