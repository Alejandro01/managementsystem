/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Data;

import java.io.Serializable;
import structures.Queue;
import structures.SLList;
import structures.Stack;

/**
 *
 * @author usuario
 */
public class Company implements Serializable{
    
    int driversCount;
    int employeesCount;
    int clientsCount;
    int productsCount;
    int cartsCount;
    
    int idCompany;
    String name;
    Employee director;
    String mail;
    Direction direction;
    SLList products;
    Queue orders;
    Stack salesHistory;
    SLList clients;
    SLList employees;
    SLList drivers;

    @Override
    public String toString() {
        return "Company{" + "idCompany=" + idCompany + ", name=" + name + ", director=" + director + ", mail=" + mail + ", direction=" + direction + ", products=" + products + ", orders=" + orders + ", salesHistory=" + salesHistory + ", clients=" + clients + ", employees=" + employees + ", drivers=" + drivers + '}';
    }
    
    public Company() {
        this.driversCount = 0;
        this.employeesCount = 0;
        this.clientsCount = 0;
        this.productsCount = 0;
        this.cartsCount = 0;
    }
    
    public Company(int idCompany, String name, Employee director, String mail, Direction direction, SLList products, Queue orders, Stack salesHistory, SLList clients, SLList employees, SLList drivers) {
        this.idCompany = idCompany;
        this.name = name;
        this.director = director;
        this.mail = mail;
        this.direction = direction;
        this.products = products;
        this.orders = orders;
        this.salesHistory = salesHistory;
        this.clients = clients;
        this.employees = employees;
        this.drivers = drivers;
        
        this.driversCount = 1;
        this.employeesCount = 1;
        this.clientsCount = 2;
        this.productsCount = 1;
        this.cartsCount = 1;
    }
    
    public int getIdCompany() {
        return idCompany;
    }

    public void setIdCompany(int idCompany) {
        this.idCompany = idCompany;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Employee getDirector() {
        return director;
    }

    public void setDirector(Employee director) {
        this.director = director;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public SLList getProducts() {
        return products;
    }

    public void setProducts(SLList products) {
        this.products = products;
    }

    public Queue getOrders() {
        return orders;
    }

    public void setOrders(Queue orders) {
        this.orders = orders;
    }

    public Stack getSalesHistory() {
        return salesHistory;
    }

    public void setSalesHistory(Stack salesHistory) {
        this.salesHistory = salesHistory;
    }

    public SLList getClients() {
        return clients;
    }

    public void setClients(SLList clients) {
        this.clients = clients;
    }

    public SLList getEmployees() {
        return employees;
    }

    public void setEmployees(SLList employees) {
        this.employees = employees;
    }

    public SLList getDrivers() {
        return drivers;
    }

    public void setDrivers(SLList drivers) {
        this.drivers = drivers;
    }

    public int getDriversCount() {
        return driversCount;
    }

    public void setDriversCount(int driversCount) {
        this.driversCount = driversCount;
    }

    public int getEmployeesCount() {
        return employeesCount;
    }

    public void setEmployeesCount(int employeesCount) {
        this.employeesCount = employeesCount;
    }

    public int getClientsCount() {
        return clientsCount;
    }

    public void setClientsCount(int clientsCount) {
        this.clientsCount = clientsCount;
    }

    public int getProductsCount() {
        return productsCount;
    }

    public void setProductsCount(int productsCount) {
        this.productsCount = productsCount;
    }

    public int getCartsCount() {
        return cartsCount;
    }

    public void setCartsCount(int cartsCount) {
        this.cartsCount = cartsCount;
    }
    
}
