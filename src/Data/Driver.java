/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Data;

import java.io.Serializable;
import java.util.Date;
import structures.SLList;


/**
 *
 * @author usuario
 */
public class Driver extends Employee implements Serializable{
    Cart shipping;
    String state;
    int truckNumberPlate;
    int truckLetterPlate;
    String truckBrand;
    SLList shipmentsHistory;

    public Driver(Cart shipping, String state, int truckNumberPlate, int truckLetterPlate, String truckBrand, SLList shipmentsHistory, String position, int salary, Date hiringDate, Employee boss, String engagementType, Schedule schedule, int idPersona, String name, String lastName, String username, String password, int age, Direction direction, int phoneNumber, String email, String gender) {
        super(position, salary, hiringDate, boss, engagementType, schedule, idPersona, name, lastName, username, password, age, direction, phoneNumber, email, gender);
        this.shipping = shipping;
        this.state = state;
        this.truckNumberPlate = truckNumberPlate;
        this.truckLetterPlate = truckLetterPlate;
        this.truckBrand = truckBrand;
        this.shipmentsHistory = shipmentsHistory;
    }

    @Override
    public String toString() {
        return "Driver{" 
                + "\nidPersona= " + idPersona 
                + "\nname= " + name 
                + "\nlastName= " + lastName 
                + "\nusername= " + username 
                + "\npassword= " + password 
                + "\nage= " + age 
                + "\ndirection= " + direction 
                + "\nphoneNumber= " + phoneNumber 
                + "\nemail= " + email 
                + "\ngender= " + gender 
                + "\nposition=" + position 
                + "\nsalary=" + salary 
                + "\nhiringDate=" + hiringDate 
                + "\nboss=" + boss 
                + "\nengagementType=" + engagementType 
                + "\nschedule=" + schedule                
                + "\nshipping=" + shipping 
                + "\nstate=" + state 
                + "\ntruckNumberPlate=" + truckNumberPlate 
                + "\ntruckLetterPlate=" + truckLetterPlate 
                + "\ntruckBrand=" + truckBrand 
                + "\nshipmentsHistory=" + shipmentsHistory + '}';
    }
    
    

    public Cart getShipping() {
        return shipping;
    }

    public void setShipping(Cart shipping) {
        this.shipping = shipping;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public int getTruckNumberPlate() {
        return truckNumberPlate;
    }

    public void setTruckNumberPlate(int truckNumberPlate) {
        this.truckNumberPlate = truckNumberPlate;
    }

    public int getTruckLetterPlate() {
        return truckLetterPlate;
    }

    public void setTruckLetterPlate(int truckLetterPlate) {
        this.truckLetterPlate = truckLetterPlate;
    }

    public String getTruckBrand() {
        return truckBrand;
    }

    public void setTruckBrand(String truckBrand) {
        this.truckBrand = truckBrand;
    }

    public SLList getShipmentsHistory() {
        return shipmentsHistory;
    }

    public void setShipmentsHistory(SLList shipmentsHistory) {
        this.shipmentsHistory = shipmentsHistory;
    }
    
}
