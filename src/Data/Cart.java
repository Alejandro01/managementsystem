/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Data;

import java.io.Serializable;
import java.util.Date;
import structures.SLList;
import structures.SLListNode;

/**
 *
 * @author usuario
 */
public class Cart implements Serializable{
    
    int idCart;
    int price;
    Date orderDate;
    SLList products;
    Direction direction;
    String state;
    
    public Cart(int idCart, int price, Date orderDate, SLList products, Direction direction, String state) {
        this.idCart = idCart;
        this.price = price;
        this.orderDate = orderDate;
        this.products = products;
        this.direction = direction;
        this.state = state;
    }

    @Override
    public String toString() {
        return "Cart{" + "idCart=" + idCart 
                + "\nprice=" + price 
                + "\norderDate=" + orderDate 
                + "\nproducts=" + products 
                + "\ndirection=" + direction 
                + "\nstate=" + state + '}';
    }
    
    

    public int getIdCart() {
        return idCart;
    }

    public void setIdCart(int idCart) {
        this.idCart = idCart;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public SLList getProducts() {
        return products;
    }

    public void setProducts(SLList products) {
        this.products = products;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
       
}
