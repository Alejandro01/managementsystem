/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Data;

import java.io.Serializable;

/**
 *
 * @author usuario
 */
public class Product implements Serializable{
    int idProduct;
    String name;
    String category;
    String description;
    int assurance;
    int price;

    public Product(int idProduct, String name, String category, String description, int assurance, int price) {
        this.idProduct = idProduct;
        this.name = name;
        this.category = category;
        this.description = description;
        this.assurance = assurance;
        this.price = price;
    }

    @Override
    public String toString() {
        return "Product{" + "idProduct=" + idProduct 
                + "\n name=" + name 
                + "\n category=" + category 
                + "\n description=" + description 
                + "\n assurance=" + assurance 
                + "\n price=" + price + '}';
    }        

    public int getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(int idProduct) {
        this.idProduct = idProduct;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getAssurance() {
        return assurance;
    }

    public void setAssurance(int assurance) {
        this.assurance = assurance;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
    
}
