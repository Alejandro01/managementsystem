/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Data;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author usuario
 */
public class Employee extends Person implements Serializable{
    String position;
    int salary;
    Date hiringDate;
    Employee boss;
    String engagementType;
    Schedule schedule;

    public Employee(String position, int salary, Date hiringDate, Employee boss, String engagementType, Schedule schedule, int idPersona, String name, String lastName, String username, String password, int age, Direction direction, int phoneNumber, String email, String gender) {
        super(idPersona, name, lastName, username, password, age, direction, phoneNumber, email, gender);
        this.position = position;
        this.salary = salary;
        this.hiringDate = hiringDate;
        this.boss = boss;
        this.engagementType = engagementType;
        this.schedule = schedule;
    }

    @Override
    public String toString() {
        return "Employee{" 
                + "\nidPersona= " + idPersona 
                + "\nname= " + name 
                + "\nlastName= " + lastName 
                + "\nusername= " + username 
                + "\npassword= " + password 
                + "\nage= " + age 
                + "\ndirection= " + direction 
                + "\nphoneNumber= " + phoneNumber 
                + "\nemail= " + email 
                + "\ngender= " + gender 
                + "\nposition=" + position 
                + "\nsalary=" + salary 
                + "\nhiringDate=" + hiringDate 
                + "\nboss=" + boss 
                + "\nengagementType=" + engagementType 
                + "\nschedule=" + schedule + '}';
    }     
    
    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public Date getHiringDate() {
        return hiringDate;
    }

    public void setHiringDate(Date hiringDate) {
        this.hiringDate = hiringDate;
    }

    public Employee getBoss() {
        return boss;
    }

    public void setBoss(Employee boss) {
        this.boss = boss;
    }

    public String getEngagementType() {
        return engagementType;
    }

    public void setEngagementType(String engagementType) {
        this.engagementType = engagementType;
    }

    public Schedule getSchedule() {
        return schedule;
    }

    public void setSchedule(Schedule schedule) {
        this.schedule = schedule;
    }
    
}

