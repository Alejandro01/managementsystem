/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Data;

import java.io.Serializable;
import java.sql.Time;
import java.util.Date;

/**
 *
 * @author Usuario
 */
public class Shipping implements Serializable{
    Cart shipping;
    Date shippingDate;
    Time deliveryTime;

    public Shipping(Cart shipping, Date shippingDate, Time deliveryTime) {
        this.shipping = shipping;
        this.shippingDate = shippingDate;
        this.deliveryTime = deliveryTime;
    }

    public Cart getShipping() {
        return shipping;
    }

    public void setShipping(Cart shipping) {
        this.shipping = shipping;
    }

    public Date getShippingDate() {
        return shippingDate;
    }

    public void setShippingDate(Date shippingDate) {
        this.shippingDate = shippingDate;
    }

    public Time getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(Time deliveryTime) {
        this.deliveryTime = deliveryTime;
    }
    
}
