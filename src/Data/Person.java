/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Data;

import java.io.Serializable;

/**
 *
 * @author usuario
 */
public class Person implements Serializable{
    int idPersona;
    String name;
    String lastName;
    String username;
    String password;
    int age;
    Direction direction;
    int phoneNumber;
    String email;
    String gender;
    
    @Override
    public boolean equals(Object obj){
        if(obj == null){
            return false;
        }
        if(getClass() != obj.getClass()){
            return false;
        }
        Person otro = (Person) obj;
        if(this.getUsername().toLowerCase().equals(otro.getUsername().toLowerCase()) && this.getPassword().equals(otro.getPassword())){
            return true;
        }else{
            return false;
        }
    }
    
    public Person(int idPersona, String name, String lastName, String username, String password, int age, Direction direction, int phoneNumber, String email, String gender) {
        this.idPersona = idPersona;
        this.name = name;
        this.lastName = lastName;
        this.username = username;
        this.password = password;
        this.age = age;
        this.direction = direction;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.gender = gender;
    }

    public int getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(int idPersona) {
        this.idPersona = idPersona;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public int getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(int phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
    
}
