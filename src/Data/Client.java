/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Data;

import java.io.Serializable;
import structures.SLList;

/**
 *
 * @author usuario
 */
public class Client extends Person implements Serializable{
    SLList carts;
    
    public Client(SLList carts, int idPersona, String name, String lastName, String username, String password, int age, Direction direction, int phoneNumber, String email, String gender) {
        super(idPersona, name, lastName, username, password, age, direction, phoneNumber, email, gender);
        this.carts = carts;
    }
    
    public SLList getCarts() {
        return carts;
    }

    public void setCarts(SLList carts) {
        this.carts = carts;
    }

    @Override
    public String toString() {   
        return "Client{ Nombre: "+ name +"Age: " + age+ "Street: "+ direction.street + "Race: "+ direction.race + "Distancia: "+ mangementsystem.MangementSystem.getDistance(age, age, 4, 4) +" }\n";
        /*
        return "Client{" + "carts= " + carts 
                + "\nidPersona= " + idPersona 
                + "\nname= " + name 
                + "\nlastName= " + lastName 
                + "\nusername= " + username 
                + "\npassword= " + password 
                + "\nage= " + age 
                + "\ndirection= Carerra " + direction
                + "\nphoneNumber= " + phoneNumber 
                + "\nemail= " + email 
                + "\ngender= " + gender 
                + '}';
                */
    }
    
    
    
}
