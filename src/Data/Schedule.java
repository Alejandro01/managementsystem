/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Data;

import java.io.Serializable;
import java.sql.Time;
import structures.SLList;

/**
 *
 * @author usuario
 */
public class Schedule implements Serializable{
    SLList days;
    Time startTime;
    Time endTime;

    public Schedule(SLList days, Time startTime, Time endTime) {
        this.days = days;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public SLList getDays() {
        return days;
    }

    public void setDays(SLList days) {
        this.days = days;
    }

    public Time getStartTime() {
        return startTime;
    }

    public void setStartTime(Time startTime) {
        this.startTime = startTime;
    }

    public Time getEndTime() {
        return endTime;
    }

    public void setEndTime(Time endTime) {
        this.endTime = endTime;
    }
    
}
