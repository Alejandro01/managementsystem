/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Data;

import java.io.Serializable;

/**
 *
 * @author usuario
 */
public class Direction implements Serializable{
    int street;
    int race;

    @Override
    public String toString() {
        return "Carrera " + race + " con calle " + street;
    }
    
    public Direction(int street, int race) {
        this.street = street;
        this.race = race;
    }

    public int getStreet() {
        return street;
    }

    public void setStreet(int street) {
        this.street = street;
    }

    public int getRace() {
        return race;
    }

    public void setRace(int race) {
        this.race = race;
    }    
}
