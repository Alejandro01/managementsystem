package controllers;

import Data.Driver;
import Data.Company;
import Data.Client;
import Data.Employee;
import Data.Product;
import Data.Direction;
import Data.Employee;
import Data.Schedule;
import Data.Cart;
import structures.Methods;
import com.sun.org.apache.bcel.internal.generic.AALOAD;
import java.io.File;
import java.io.IOException;
import views.View;
import java.util.Date;
import static mangementsystem.MangementSystem.cargarInfo;
import static mangementsystem.MangementSystem.guardarInfo;
import structures.Queue;
import structures.SLList;
import structures.Stack;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import static java.util.Collections.list;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import structures.SLListNode;

public class Controller {

    public static final int ROL_OPTION_ONE_FOR_EMPLOYEE = 1;
    public static final int MENU_OPTION_TWO_FOR_CLIENT = 2;
    
    public static final int MENU_OPTION_CLIENT_ONE_FOR_LOGIN = 1;
    public static final int MENU_OPTION_CLIENT_TWO_FOR_SIGN = 2;
    public static final int MENU_OPTION_CLIENT_THREE_TO_RETURN = 3;
    
    public static final int MENU_OPTION_EMPLOYEE_ONE_FOR_LOGIN = 1;
    public static final int MENU_OPTION_EMPLOYEE_TWO_TO_RETURN = 2;
    
    public static final int MENU_OPTION_ONE_FOR_ADD_CLIENT = 1;
    public static final int MENU_OPTION_TWO_FOR_SHOW_CLIENT = 2;
    public static final int MENU_OPTION_THREE_FOR_UPDATE_CLIENT = 3;
    public static final int MENU_OPTION_FOUR_FOR_DELETE_CLIENT = 4;

    public static final int MENU_OPTION_ONE_FOR_ADD_EMPLOYEE = 1;
    public static final int MENU_OPTION_TWO_FOR_SHOW_EMPLOYEE = 2;
    public static final int MENU_OPTION_THREE_FOR_UPDATE_EMPLOYEE = 3;
    public static final int MENU_OPTION_FOUR_FOR_DELETE_EMPLOYEE = 4;

    public static final int MENU_OPTION_FOUR_FOR_EXIT = 4;
    public static final int MENU_OPTION_FIVE_FOR_EXIT = 5;
     
    public static final int MENU_OPTION_ONE_FOR_SORT_CLIENT_BY_AGE = 1;
    public static final int MENU_OPTION_TWO_FOR_SORT_CLIENT_BY_DISTANCE = 2;
    public static final int MENU_OPTION_THREE_FOR_SORT_CLIENT_BY_NAME = 3;

    private int activeIdUser;
    private String activeTypeUser;
    private static View view;
    private static Company company = new Company();
    private static Methods methods = new Methods();

    public Controller() {
        view = new View();
        //init();                
    }

    private void rol(Controller xd) {
        int userOption = view.getRolOption();
        switch (userOption) {
            case ROL_OPTION_ONE_FOR_EMPLOYEE:
                xd.initEmployee(xd);
            break;
            case MENU_OPTION_TWO_FOR_CLIENT:
                xd.initClient(xd);
            break;
            default:
                xd.rol(xd);
                break;
        }
    }
    
    private void initEmployee(Controller xd) {
        int userOption = view.getMenuOptionEmployee();
        switch (userOption) {
            case MENU_OPTION_EMPLOYEE_ONE_FOR_LOGIN:
                xd.loginEmployee(xd);
                break;
            case MENU_OPTION_EMPLOYEE_TWO_TO_RETURN:
                xd.rol(xd);
                break;
            default:
                view.printErrorMessage();
                xd.initEmployee(xd);
                break;
        }
    }
    
    private void initClient(Controller xd) {
        int userOption = view.getMenuOptionClient();
        switch (userOption) {
            case MENU_OPTION_CLIENT_ONE_FOR_LOGIN:
                xd.loginClient(xd);
                break;
            case MENU_OPTION_CLIENT_TWO_FOR_SIGN:
                xd.createClient();
                xd.loginClient(xd);
                break;
            case MENU_OPTION_CLIENT_THREE_TO_RETURN:
                xd.rol(xd);
                break;
            default:
                view.printErrorMessage();
                xd.initClient(xd);
                break;
        }
    }

    private void controllerEmployee(Controller xd){
        int userOption = view.getControllerEmployeeMenuOption();
        switch (userOption) {
            case MENU_OPTION_ONE_FOR_ADD_EMPLOYEE:
                xd.createEmployee();
                break;
            case MENU_OPTION_TWO_FOR_SHOW_EMPLOYEE:
                xd.readEmployee(view.getId());
                break;
            case MENU_OPTION_THREE_FOR_UPDATE_EMPLOYEE:
                xd.updateEmployee(view.getId());
                break;
            case MENU_OPTION_FOUR_FOR_DELETE_EMPLOYEE:
                xd.deleteEmployee(view.getId());
                break;
            case MENU_OPTION_FIVE_FOR_EXIT:
                return;
            default:
                view.printErrorMessage();
                xd.controllerEmployee(xd);
                break;
        }
    }
    
    private void controllerClient(Controller xd) {
        int userOption = view.getControllerClientMenuOption();
        switch (userOption) {
            case MENU_OPTION_ONE_FOR_ADD_CLIENT:
                xd.createClient();
                break;
            case MENU_OPTION_TWO_FOR_SHOW_CLIENT:
                xd.readClient(view.getId());
                break;
            case MENU_OPTION_THREE_FOR_UPDATE_CLIENT:
                xd.updateClient(view.getId());
                break;
            case MENU_OPTION_FOUR_FOR_DELETE_CLIENT:
                xd.deleteClient(view.getId());
                break;
            case MENU_OPTION_FIVE_FOR_EXIT:
                return;
            default:
                view.printErrorMessage();
                xd.initClient(xd);
                break;
        }
        initClient(xd);
    }

    private void sortClients(Controller xd){
        int userOption = view.getSortClientMenuOption();
        switch (userOption) {
            case MENU_OPTION_ONE_FOR_SORT_CLIENT_BY_AGE :
                mangementsystem.MangementSystem.orderClient(company, methods, "AGE");
                break;
            case MENU_OPTION_TWO_FOR_SORT_CLIENT_BY_DISTANCE:
                mangementsystem.MangementSystem.orderClient(company, methods, "DISTANCE");
                break;
            case MENU_OPTION_THREE_FOR_SORT_CLIENT_BY_NAME:
                mangementsystem.MangementSystem.orderClient(company, methods, "NAME");
                break;
            case MENU_OPTION_FOUR_FOR_EXIT:
                return;
            default:
                view.printErrorMessage();
                xd.sortClients(xd);
        }
    }
    
    public boolean isEmployee(String username, String password) {
        return true;
    }

    public boolean isClient(String username, String password) {
        return true;
    }

    private void createClient() {
        //SLList carts = null;
        int clientId = company.getClientsCount();
        String name = view.getName();
        String lastName = view.getLastName();
        String username = view.getUsername();
        String password = view.getPassword();
        int age = view.getAge();
        Direction direction = new Direction(view.getStreet(), view.getStreetPerpendicular());
        int phoneNumber = view.getPhoneNumber();
        String email = view.getEmail();
        String gender = view.getSex();
        company.getClients().insertTail(clientId, new Client(null, clientId, name, lastName, username, password, age, direction, phoneNumber, email, gender));
        System.out.println("El Id del cliente será: " + clientId);
        company.setClientsCount(company.getClientsCount() + 1);
    }

    private void readClient(int clientId) {
        Client client = (Client) company.getClients().readNode(company.getClients().findUsingKey(clientId));
        System.out.println(client.toString());
    }

    private void updateClient(int clientId) {
        //SLList carts = null;                
        String name = view.getName();
        String lastName = view.getLastName();
        String username = view.getUsername();
        String password = view.getPassword();
        int age = view.getAge();
        Direction direction = new Direction(view.getStreet(), view.getStreetPerpendicular());
        int phoneNumber = view.getPhoneNumber();
        String email = view.getEmail();
        String gender = view.getSex();
        company.getClients().setAt(clientId, new Client(null, clientId, name, lastName, username, password, age, direction, phoneNumber, email, gender));
    }

    private void deleteClient(int clientId) {
        company.getClients().removeAt(company.getClients().findUsingKey(clientId));
    }

    private void createEmployee() {
        try {
            String position = view.getEmployeePosition();
            int salary = view.getSalary();
            Date hiringDate = new SimpleDateFormat("dd/MM/yyyy").parse(view.getHiringDate());
            //boss
            String engagementType = view.getEngagementType();
            //schedule
            int employeeId = company.getEmployeesCount();
            String name = view.getName();
            String lastName = view.getLastName();
            String username = view.getUsername();
            String password = view.getPassword();
            int age = view.getAge();
            Direction direction = new Direction(view.getStreet(), view.getStreetPerpendicular());
            int phoneNumber = view.getPhoneNumber();
            String email = view.getEmail();
            String gender = view.getSex();
            company.getEmployees().insertTail(employeeId, new Employee(position, salary, hiringDate, null, engagementType, null, employeeId, name, lastName, username, password, age, direction, phoneNumber, email, gender));
            System.out.println("El Id del empleado será" + company.getEmployeesCount());
            company.setEmployeesCount(company.getEmployeesCount() + 1);
        } catch (ParseException ex) {
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void readEmployee(int employeeId) {
        Employee employee = (Employee) company.getEmployees().readNode(company.getEmployees().findUsingKey(employeeId));
        System.out.println(employee.toString());
    }

    private void updateEmployee(int employeeId) {
        try {
            String position = view.getEmployeePosition();
            int salary = view.getSalary();
            Date hiringDate = new SimpleDateFormat("dd/MM/yyyy").parse(view.getHiringDate());
            //boss
            String engagementType = view.getEngagementType();
            //schedule
            String name = view.getName();
            String lastName = view.getLastName();
            String username = view.getUsername();
            String password = view.getPassword();
            int age = view.getAge();
            Direction direction = new Direction(view.getStreet(), view.getStreetPerpendicular());
            int phoneNumber = view.getPhoneNumber();
            String email = view.getEmail();
            String gender = view.getSex();
            company.getEmployees().setAt(employeeId, new Employee(position, salary, hiringDate, null, engagementType, null, employeeId, name, lastName, username, password, age, direction, phoneNumber, email, gender));
        } catch (ParseException ex) {
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void deleteEmployee(int employeeId) {
        company.getEmployees().removeAt(company.getEmployees().findUsingKey(employeeId));
    }

    private void createDriver() {
        try {
            //Cart shipping;
            String state = view.getDriverState();
            int truckNumberPlate = view.getTruckPlateNumber();
            int truckLetterPlate = view.getTruckPlateLetter();
            String truckBrand = view.getTruckBrand();
            //SLList shipmentsHistory;
            String position = "conductor";
            int salary = view.getSalary();
            Date hiringDate = new SimpleDateFormat("dd/MM/yyyy").parse(view.getHiringDate());
            //boss
            String engagementType = view.getEngagementType();
            //schedule
            int driverId = company.getDriversCount();
            String name = view.getName();
            String lastName = view.getLastName();
            String username = view.getUsername();
            String password = view.getPassword();
            int age = view.getAge();
            Direction direction = new Direction(view.getStreet(), view.getStreetPerpendicular());
            int phoneNumber = view.getPhoneNumber();
            String email = view.getEmail();
            String gender = view.getSex();
            company.getDrivers().insertTail(driverId, new Driver(null, state, truckNumberPlate, truckLetterPlate, truckBrand, null, position, salary, hiringDate, null, engagementType, null, driverId, name, lastName, username, password, age, direction, phoneNumber, email, gender));
            System.out.println("El Id del conductor será" + company.getDriversCount());
            company.setDriversCount(company.getDriversCount() + 1);
        } catch (ParseException ex) {
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void readDriver(int driverId) {
        Driver driver = (Driver) company.getDrivers().readNode(company.getDrivers().findUsingKey(driverId));
        System.out.println(driver.toString());
    }

    private void updateDriver(int driverId) {
        try {
            //Cart shipping;
            String state = view.getDriverState();
            int truckNumberPlate = view.getTruckPlateNumber();
            int truckLetterPlate = view.getTruckPlateLetter();
            String truckBrand = view.getTruckBrand();
            //SLList shipmentsHistory;
            String position = "conductor";
            int salary = view.getSalary();
            Date hiringDate = new SimpleDateFormat("dd/MM/yyyy").parse(view.getHiringDate());
            //boss
            String engagementType = view.getEngagementType();
            //schedule
            String name = view.getName();
            String lastName = view.getLastName();
            String username = view.getUsername();
            String password = view.getPassword();
            int age = view.getAge();
            Direction direction = new Direction(view.getStreet(), view.getStreetPerpendicular());
            int phoneNumber = view.getPhoneNumber();
            String email = view.getEmail();
            String gender = view.getSex();
            company.getClients().setAt(driverId, new Driver(null, state, truckNumberPlate, truckLetterPlate, truckBrand, null, position, salary, hiringDate, null, engagementType, null, driverId, name, lastName, username, password, age, direction, phoneNumber, email, gender));
        } catch (ParseException ex) {
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void deleteDriver(int driverId) {
        company.getDrivers().removeAt(company.getDrivers().findUsingKey(driverId));
    }

    private void createProduct() {
        int idProduct = company.getProductsCount();
        String name = view.getName();
        String category = view.getProductCategory();
        String description = view.getProductDescription();
        int assurance = view.getProductAssurance();
        int price = view.getPrice();
        company.getProducts().insertTail(idProduct, new Product(idProduct, name, category, description, assurance, price));
        System.out.println("El Id del producto será" + company.getProductsCount());
        company.setProductsCount(company.getProductsCount() + 1);
    }

    private void readProduct(int productId) {
        Product driver = (Product) company.getProducts().readNode(company.getProducts().findUsingKey(productId));
        System.out.println(driver.toString());
    }

    private void updateProduct(int productId) {
        String name = view.getName();
        String category = view.getProductCategory();
        String description = view.getProductDescription();
        int assurance = view.getProductAssurance();
        int price = view.getPrice();
        company.getProducts().setAt(productId, new Product(productId, name, category, description, assurance, price));
    }

    private void deleteProduct(int productId) {
        company.getProducts().removeAt(company.getProducts().findUsingKey(productId));
    }

    private void createCart() {
        int cartId = company.getCartsCount();
        int price = view.getPrice();
        ZoneId defaultZoneId = ZoneId.systemDefault();
        LocalDate currentDate = LocalDate.now();
        Date orderDate = Date.from(currentDate.atStartOfDay(defaultZoneId).toInstant());
        //SLList products = ;
        Direction direction = new Direction(view.getCartStreet(), view.getCartStreetPerpendicular());
        String state = view.getCartState();
//            company.getOrders().push(cartId, new Cart(cartId, price, orderDate, products, direction, state));
        System.out.println("El Id del carrito será" + company.getCartsCount());
        company.setCartsCount(company.getCartsCount() + 1);
    }

    //private void readCompany() {
      //  System.out.print(company);
    //}

    private void updateCompany() throws ParseException {
        String name = view.getCompanyName();
        Employee director = new Employee("Director", 15000000, new SimpleDateFormat("dd/MM/yyyy").parse("07/07/2050/n"), null, "planta", null, 0, "Jhon", "Smith", "jhon", "QWEQWE", 65, new Direction(34, 65), 313434333, "jhon@", "hombre");
        String mail = view.getCompanyMail();
        Direction direction = new Direction(view.getCompanyStreet(), view.getCompanyStreetPerpendicular());
        SLList products = new SLList();
        Queue orders = new Queue();
        Stack salesHistory = new Stack();
        SLList clients = new SLList();
        SLList employees = new SLList();
        SLList drivers = new SLList();
        company.setName(name);
        company.setDirector(director);
        company.setMail(mail);
        company.setDirection(direction);
        company.setProducts(products);
        company.setOrders(orders);
        company.setSalesHistory(salesHistory);
        company.setClients(clients);
        company.setEmployees(employees);
        int idEmployees = company.getEmployeesCount();
        company.getEmployees().insertTail(idEmployees, director);
        company.setEmployeesCount(company.getEmployeesCount() + 1);
        company.setDrivers(drivers);
    }

    private void loginEmployee(Controller xd) {
        String usename = view.getUsername();
        String password = view.getPassword();
        if (mangementsystem.MangementSystem.checkEmployee(usename, password, company) == -1) {
            view.printErrorUser();
            xd.initEmployee(xd);
        } else {
            System.out.println("usuario correcto");
            xd.activeIdUser = mangementsystem.MangementSystem.checkEmployee(usename, password, company);
            xd.activeTypeUser = "EMPLOYEE";
        }
    }
    
    private void loginClient(Controller xd) {
        String usename = view.getUsername();
        String password = view.getPassword();
        if (mangementsystem.MangementSystem.checkClient(usename, password, company) == -1) {
            view.printErrorUser();
            xd.initClient(xd);
        } else {
            xd.activeIdUser = mangementsystem.MangementSystem.checkClient(usename, password, company);
            xd.activeTypeUser = "CLIENT";
        }
    }

    public static void main(String[] args) throws IOException, ClassNotFoundException, ParseException {
        Controller xd = new Controller();
        view.welcome();
        // serializacion
        File fichero = new File("datos.txt");
        if (!fichero.exists()) {
            xd.updateCompany();
            mangementsystem.MangementSystem.guardarInfo(company);
        }
        company = cargarInfo();
        //System.out.println(" direccion: "+company.getDirection().getRace() + "   " + company.getDirection().getStreet());
        // Desarrollo de funcionalidades
        xd.rol(xd);
        if(xd.activeTypeUser == "EMPLOYEE"){
            xd.controllerClient(xd);
            xd.controllerEmployee(xd);
            // distribuir pedidos
            xd.sortClients(xd);
        }else{
            // funciones del cliente
        }
              
        //for(int i = 0; i < 100; i++){
          //xd.createClient();
        //}
        System.out.println(company.getClients().size+"   "+company.getClientsCount());
        mangementsystem.MangementSystem.guardarInfo(company);
    }
}
